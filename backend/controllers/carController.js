const asyncHandler = require('express-async-handler')

const Car = require('../models/carModel')
const User = require('../models/userModel')

// @desc    Get all cars
// @route    GET /api/cars
// @access    Private
const getCars = asyncHandler(async (req, res) => {
    const cars = await Car.find()
    res.status(200).json(cars)
})

// @desc    Get all cars
// @route    GET /api/cars
// @access    Private
const getAvailableCars = asyncHandler(async (req, res) => {
    const cars = await Car.find({isAvailable: true})
    if (cars.length === 0) {
        return res.status(200).json({message: "No cars available at the moment"})
    }
    res.status(200).json(cars)
})

// @desc    Set car
// @route    POST /api/cars
// @access    Private
const setCar = asyncHandler(async (req, res) => {
    const { body } = req
    if (!body.model || !body.brand || !body.yearOfFabrication || !body.plate || !body.color || !body.chassi || !body.purchaseValue) {
        res.status(400)
        throw new Error('Please provide all the required informations')

    }
    const newCar = {
        model: body.model,
        brand: body.brand,
        yearOfFabrication: body.yearOfFabrication,
        plate: body.plate,
        color: body.color,
        chassi: body.chassi,
        purchaseValue: body.purchaseValue,
        user: req.user.id
    }

    const createdCar = await Car.create(newCar)

    res.status(200).json(createdCar)
})

// @desc    Update car
// @route    PUT /api/cars/:id
// @access    Private
const updateCar = asyncHandler(async (req, res) => {
    const car = await Car.findById(req.params.id)

    if (!car) {
        res.status(400)
        throw new Error('Car not found')
    }

    // Check for user
    if(!req.user) {
        res.status(401)
        throw new Error('User not found')
    }

    // Make sure the logged in user matches the sell user
    if (car.user.toString() !== req.user.id) {
        res.status(401)
        throw new Error('User not authorized')
    }

    const updatedCar = await Car.findByIdAndUpdate(req.params.id, req.body)
    res.status(200).json(updatedCar)
})

// @desc    Delete car
// @route    DELETE /api/cars/:id
// @access    Private
const deleteCar = asyncHandler(async (req, res) => {
    const car = await Car.findById(req.params.id)

    if (!car) {
        res.status(400)
        throw new Error('Car not found')
    }

    // Check for user
    if(!req.user) {
        res.status(401)
        throw new Error('User not found')
    }

    // Make sure the logged in user matches the sell user
    if (car.user.toString() !== req.user.id) {
        res.status(401)
        throw new Error('User not authorized')
    }

    await car.remove()
    res.status(200).json({id: req.params.id})
})

module.exports = {
    getCars,
    getAvailableCars,
    setCar,
    updateCar,
    deleteCar
}