const asyncHandler = require('express-async-handler')

const Sell = require('../models/sellModel')
const Car = require('../models/carModel')
const User = require('../models/userModel')

// @desc    Get sell
// @route    GET /api/sells
// @access    Private
const getSells = asyncHandler(async (req, res) => {
    const sells = await Sell.find().populate('car', 'model brand yearOfFabrication -_id').select('user car sellValue')
    res.status(200).json(sells)
})

// @desc    Set sell
// @route    POST /api/sells
// @access    Private
const setSell = asyncHandler(async (req, res) => {
    const { body } = req
    if (!body.sellValue) {
        res.status(400)
        throw new Error('Please provide all the required informations')

    }

    const carSold = await Car.findById(body.car)
    if (!carSold) {
        res.status(400)
        throw new Error('Car does not exist')
        
    }
    if (carSold.isAvailable === false) {
        res.status(400)
        throw new Error('Car already sold')
    }
    console.log(carSold._id);
    const newSell = {
        sellValue: body.sellValue,
        car: body.car,
        user: req.user.id
    }

    const createdSell = await Sell.create(newSell)
    carSold.isAvailable = false
    carSold.user = body.car
    await carSold.save()
    res.status(200).json(createdSell)
})

// @desc    Update sell
// @route    PUT /api/sells/:id
// @access    Private
const updateSell = asyncHandler(async (req, res) => {
    const sell = await Sell.findById(req.params.id)

    if (!sell) {
        res.status(400)
        throw new Error('Sell not found')
    }

    // Check for user
    if(!req.user) {
        res.status(401)
        throw new Error('User not found')
    }

    // Make sure the logged in user matches the sell user
    if (sell.user.toString() !== req.user.id) {
        res.status(401)
        throw new Error('User not authorized')
    }

    const updatedSell = await Sell.findByIdAndUpdate(req.params.id, req.body)
    res.status(200).json(updatedSell)
})

// @desc    Delete car
// @route    DELETE /api/cars/:id
// @access    Private
const deleteSell = asyncHandler(async (req, res) => {
    const sell = await Sell.findById(req.params.id)

    if (!sell) {
        res.status(400)
        throw new Error('Sell not found')
    }

    // Check for user
    if(!req.user) {
        res.status(401)
        throw new Error('User not found')
    }

    // Make sure the logged in user matches the sell user
    if (sell.user.toString() !== req.user.id) {
        res.status(401)
        throw new Error('User not authorized')
    }

    await sell.remove()
    res.status(200).json({id: req.params.id})
})

module.exports = {
    getSells,
    setSell,
    updateSell,
    deleteSell
}