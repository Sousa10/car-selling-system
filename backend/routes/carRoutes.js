const express = require('express')
const router = express.Router()
const {
    getCars,
    getAvailableCars,
    setCar,
    updateCar,
    deleteCar
} = require('../controllers/carController')

const {protect} = require('../middleware/authMiddleware')

router.route('/').get(protect, getCars).post(protect, setCar)
router.route('/available-cars').get(getAvailableCars)
router.route('/:id').delete(protect, deleteCar).put(protect, updateCar)

module.exports = router