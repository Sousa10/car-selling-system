const express = require('express')
const router = express.Router()
const {
    getSells,
    setSell,
    updateSell,
    deleteSell
} = require('../controllers/sellController')

const {protect} = require('../middleware/authMiddleware')

router.route('/').get(protect, getSells).post(protect, setSell)
router.route('/:id').delete(protect, deleteSell).put(protect, updateSell)

module.exports = router