const mongoose = require('mongoose')

const sellSchema = mongoose.Schema({
    car: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Car'
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    sellValue: {
        type: Number,
        required: [true, 'Please add a value']
    },
    sellDate: {
        type: Date,
        default: Date.now
    }
},
    {
        timestamps: true
    }
)

module.exports = mongoose.model('Sell', sellSchema)