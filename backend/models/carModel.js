const mongoose = require('mongoose')

const carSchema = mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    model: {
        type: String,
        required: [true, 'Please add a model']
    },
    brand: {
        type: String,
        required: [true, 'Please add a brand']
    },
    yearOfFabrication: {
        type: String,
        required: [true, 'Please add the year of fabrication']
    },
    plate: {
        type: String,
        required: [true, 'Please add a plate']
    },
    color: {
        type: String,
        required: [true, 'Please add a color']
    },
    chassi: {
        type: String,
        required: [true, 'Please add a chassi']
    },
    purchaseDate: {
        type: Date,
        default: Date.now
    },
    purchaseValue: {
        type: Number,
        required: [true, 'Please add a value']
    },
    isAvailable: {
        type: Boolean,
        default: true
    }
},
    {
        timestamps: true
    }
)

module.exports = mongoose.model('Car', carSchema)