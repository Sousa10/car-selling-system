# Car Selling System

Sistema para agência de veículos que permite:

- Cadastrar a compra de um veículo, modelo, marca, ano de fabricação, placa, cor, chassi, data da compra e valor da compra.

- Registrar a venda de um veículo, com data da venda, valor da venda.

- Listar todos os veículos, veículos disponíveis e histórico de veículos vendidos.

O sistema permite o registo e a autenticação de utilizadores e conta também com a proteção dos endpoints através do JWT(https://jwt.io/introduction).


# Persistência de dados

Para persistir os dados foi utilizado uma base de dados mongo que está na cloud (Mongo Atlas)

## Backend framework

Express(https://expressjs.com/)

## Frontend framework

React(https://reactjs.org/)

## Rodar a app
Para rodar a app ir na raiz do projeto e correr o comando docker-compose up.

## Testar endpoints
Para testar a API ir na pasta backend no terminal e executar npm run server. Depois com o POSTMAN executar os seguintes requests: https://documenter.getpostman.com/view/21243053/2s83YZjj75

## Nota
Os endpoints estão protegidos pelo que primeiro deve registrar um utilizador ou fazer o login para poder ter o token que é utilizado para ter acesso.

