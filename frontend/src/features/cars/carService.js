import axios from "axios";

const API_URL = "http://localhost:5000/api/cars/";

// Create new car
const createCar = async (carData, token) => {
  const config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };

  const response = await axios.post(API_URL, carData, config);

  return response.data;
};

// Get cars
const getCars = async (token) => {
  const config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };

  const response = await axios.get(API_URL, config);

  return response.data;
};

const carService = {
  createCar,
  getCars,
};

export default carService;
