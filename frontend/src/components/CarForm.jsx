import { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {createCar} from '../features/cars/carSlice'

function CarForm() {
    const [formData, setFormData] = useState({
        model: '',
        brand: '',
        yearOfFabrication: '',
        plate: '',
        color: '',
        chassi: '',
        purchaseValue: 0
    })

    const dispatch = useDispatch()

    const { model, brand, yearOfFabrication, plate, color, chassi, purchaseValue} = formData

    const onSubmit = e => {
        e.preventDefault()

        const carData = {
            model, 
            brand, 
            yearOfFabrication, 
            plate, 
            color, 
            chassi, 
            purchaseValue
          }

        dispatch(createCar(carData))
        setFormData({
          model: '',
          brand: '',
          yearOfFabrication: '',
          plate: '',
          color: '',
          chassi: '',
          purchaseValue: 0
          })
    }

    const onChange = (e) => {
        setFormData((prevState) => ({
          ...prevState,
          [e.target.name]: e.target.value
        }))
    }

    return (
        <section>
            <form onSubmit={onSubmit}>
                <div className="form-group">
                    <label htmlFor="model">Model</label>
                    <input type="text" name='model' id='model' value={model} onChange={onChange} />
                    <label htmlFor="model">Brand</label>
                    <input type="text" name='brand' id='brand' value={brand} onChange={onChange} />
                    <label htmlFor="model">Year Of Fabrication</label>
                    <input type="text" name='yearOfFabrication' id='yearOfFabrication' value={yearOfFabrication} onChange={onChange} />
                    <label htmlFor="model">Plate</label>
                    <input type="text" name='plate' id='plate' value={plate} onChange={onChange} />
                    <label htmlFor="model">Color</label>
                    <input type="text" name='color' id='color' value={color} onChange={onChange} />
                    <label htmlFor="model">Chassi</label>
                    <input type="text" name='chassi' id='chassi' value={chassi} onChange={onChange} />
                    <label htmlFor="model">Purchase Value</label>
                    <input type="number" name='purchaseValue' id='purchaseValue' value={purchaseValue} onChange={onChange} />
                    <div className="form-group">
                        <button className="btn btn-block" type='submit'>Purchase Car</button>
                    </div>
                </div>
            </form>
        </section>
    )
}

export default CarForm