function CarItem({car}) {
  return (
    <div className="car">
        <div>
            {new Date(car.createdAt).toLocaleString('en-US')}
        </div>
        <h2>{car.model}</h2>
        <h2>{car.brand}</h2>
        <h2>{car.yearOfFabrication}</h2>
        <h2>{car.plate}</h2>
        <h2>{car.color}</h2>
        <h2>{car.chassi}</h2>
        <h2>{car.purchaseValue}</h2>
    </div>
  )
}

export default CarItem